## Makefile downloads from git repository and extracts ttf/otf files. Use
## createdeb.sh to build debian package.

otfsrc=https://github.com/google/material-design-icons/archive/refs/tags/4.0.0.tar.gz
otfout=$(shell basename ${otfsrc})

all:
	test -f ${otfout} || wget ${otfsrc}
	tar xzvf ${otfout} material-design-icons-4.0.0/font
	mv material-design-icons-4.0.0/font otf
	rmdir material-design-icons-4.0.0

clean:
	rm -f ${otfout}
	rm -rf otf
