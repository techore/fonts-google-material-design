## Material Icons

Create unofficial Debian package for Material Icons font.

### About

This repository provides a simple way to create a .deb font package. 

### Installation

1. `make` to download and unpack fonts
2. `createdeb.sh` to create deb package
3. `dpkg -i packagename.deb` to install package
